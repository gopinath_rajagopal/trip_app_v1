package com.trip.dao;

import java.util.List;

import com.trip.model.Trip;
import com.trip.model.Users;

public interface UserDAO {

	String SaveUser(Users user);
	Trip saveTrip(Trip trip);
	Trip getTrip(String trip);
	List<Trip> getAllTrip(String email);
}
