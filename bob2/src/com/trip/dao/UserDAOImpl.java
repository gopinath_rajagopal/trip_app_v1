package com.trip.dao;

import java.security.SecureRandom;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.trip.model.Trip;
import com.trip.model.Users;

public class UserDAOImpl implements UserDAO {
	@Autowired
	MongoOperations mongoOperation;
	
	@Override
	public String SaveUser(Users user) {
		SecureRandom random = new SecureRandom();
	    byte bytes[] = new byte[100];
	    random.nextBytes(bytes);
	    user.setToken(bytes.toString());
	Query searchUserQuery = new Query(Criteria.where("email").is(user.getEmail()));
	List<Users> savedUser = mongoOperation.find(searchUserQuery, Users.class);
	if(savedUser.size()==0){
		mongoOperation.save(user);	
		return user.getToken();
	}
	else{
		return user.getToken();
	}		
	}

	@Override
	public Trip saveTrip(Trip trip) {
		if(trip.getId()==null){
			SecureRandom random = new SecureRandom();
		    byte bytes[] = new byte[100];
		    random.nextBytes(bytes);
		    trip.setId(bytes.toString());
		    mongoOperation.save(trip);
		    return trip;
		}
		else{
			Update s = new Update();
			s.set("userEmail", trip.getUserEmail());
			s.set("_id", trip.getId());
			s.set("userEmail", trip.getUserEmail());
			s.set("name", trip.getName());
			s.set("aboutTrip", trip.getAboutTrip());
			s.set("fromDate", trip.getFromDate());
			s.set("toDate", trip.getToDate());
			s.set("destination", trip.getDestination());
			s.set("friends", trip.getFriends());
			s.set("messageToFriends", trip.getMessageToFriends());
			s.set("suggestion", trip.getSuggestion());
			s.set("discussion", trip.getDiscussion());
			Query searchUserQuery = new Query(Criteria.where("id").is(trip.getId()));
			mongoOperation.updateFirst(searchUserQuery, s,Trip.class );
			List<Trip> triptmp = mongoOperation.find(searchUserQuery, Trip.class);
			if(triptmp.size()>0){
			 trip=	triptmp.get(0);
			}
			return trip;	
		}
		
	}

	@Override
	public Trip getTrip(String trip) {
		Trip retVal = new Trip();
		Query searchUserQuery = new Query(Criteria.where("id").is(trip));
		List<Trip> getuser = mongoOperation.find(searchUserQuery, Trip.class);
		if(getuser.size()>0){
			retVal= getuser.get(0);
		}
		return retVal;
	}

	@Override
	public List<Trip> getAllTrip(String email) {
	    Query searchFriends = new Query((Criteria.where("friends.email").is(email)));
	    List<Trip> friendsTrip = mongoOperation.find(searchFriends, Trip.class);
	    
		return friendsTrip;
	}
}
