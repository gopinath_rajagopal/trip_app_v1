package com.trip.deligate;

import java.util.List;

import com.trip.model.Trip;
import com.trip.model.Users;



public interface TripDeligate{
	 
	String save(Users user);
	Trip saveTrip(Trip trip);
	Trip getTrip(String trip);
	List<Trip> getAllTrip(String email);
}
