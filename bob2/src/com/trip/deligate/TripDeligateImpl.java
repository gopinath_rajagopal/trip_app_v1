package com.trip.deligate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.trip.dao.UserDAO;
import com.trip.model.Trip;
import com.trip.model.Users;
public class TripDeligateImpl implements TripDeligate {
	@Autowired
	UserDAO userDao;
	public String save(Users user) {
		return userDao.SaveUser(user);
	}
	public UserDAO getUserDao() {
		return userDao;
	}
	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}
	@Override
	public Trip saveTrip(Trip trip) {
	return userDao.saveTrip(trip);
		
	}
	@Override
	public Trip getTrip(String trip) {
		return userDao.getTrip(trip);
	}
	@Override
	public List<Trip> getAllTrip(String email) {
		// TODO Auto-generated method stub
		return userDao.getAllTrip(email);
	}

}