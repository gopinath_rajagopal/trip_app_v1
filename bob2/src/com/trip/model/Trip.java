package com.trip.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "trip_details")
@XmlRootElement
public class Trip {

	@Id
	@XmlElement 
	private String id;
	
	@XmlElement 
	private String userEmail;
	  @XmlElement 
	private String name;
	  @XmlElement 
	private String aboutTrip;
	  @XmlElement 
	private String fromDate;
	  @XmlElement 
	private String toDate;
	  @XmlElement 
	private String destination;
	  @XmlElement 
	private List<Friends> friends;
	  @XmlElement 
	private String messageToFriends;
	  @XmlElement 
    private List<Suggestion> suggestion;
	  @XmlElement 
    private List<Discussion> discussion;
	  
	public List<Suggestion> getSuggestion() {
		return suggestion;
	}
	public void setSuggestion(List<Suggestion> suggestion) {
		this.suggestion = suggestion;
	}
	public List<Discussion> getDiscussion() {
		return discussion;
	}
	public void setDiscussion(List<Discussion> discussion) {
		this.discussion = discussion;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String tripName) {
		this.name = tripName;
	}
	public String getAboutTrip() {
		return aboutTrip;
	}
	public void setAboutTrip(String aboutTrip) {
		this.aboutTrip = aboutTrip;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public List<Friends> getFriends() {
		return friends;
	}
	public void setFriends(List<Friends> friends) {
		this.friends = friends;
	}
	public String getMessageToFriends() {
		return messageToFriends;
	}
	public void setMessageToFriends(String messageToFriends) {
		this.messageToFriends = messageToFriends;
	}
	  public String getUserEmail() {
			return userEmail;
		}
		public void setUserEmail(String userEmail) {
			this.userEmail = userEmail;
		}
}
