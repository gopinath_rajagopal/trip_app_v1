package com.trip.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.trip.deligate.TripDeligate;
import com.trip.model.Trip;
import com.trip.model.Users;
@Component
@Path("/TokenService")
public class TokenServiceImpl {
	@Autowired
	TripDeligate tripDeligate;
	
	@POST
	@Path("/tokens")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getTokens(Users user) {
	return tripDeligate.save(user);
	}
	
	@POST
	@Path("/trip")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Trip createTrip(Trip trip) {
	return tripDeligate.saveTrip(trip);
	}
	
	@POST  
	@Path("/gettrip")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Trip getTrip(Trip trip) {
	return tripDeligate.getTrip(trip.getId());
	}
	
	@POST  
	@Path("/getalltrips")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Trip> getAllTrip(Users email) {
	return tripDeligate.getAllTrip(email.getEmail());
	}
	
}