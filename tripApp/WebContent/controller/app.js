var app = angular.module('CiulApp', ['facebook','ngCookies','jackrabbitsgroup.angular-google-auth','ngBootstrap']);

  app.config([
    'FacebookProvider',
    function(FacebookProvider) {
     var myAppId = '486805018137448';
     
     // You can set appId with setApp method
     // FacebookProvider.setAppId('myAppId');
     
     /**
      * After setting appId you need to initialize the module.
      * You can pass the appId on the init method as a shortcut too.
      */
     FacebookProvider.init(myAppId);
     
    }
  ]);
  