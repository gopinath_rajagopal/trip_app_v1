app.controller('MainController', [
    '$scope',
    '$timeout',
    '$cookieStore',
    'Facebook','$http','jrgGoogleAuth',
    function($scope, $timeout, $cookieStore, Facebook,$http,jrgGoogleAuth) {
         $scope.user = {};
         $scope.tripSaved =false;
         $scope.userTrips =[];
         if($cookieStore.get('user')!=null){
              $scope.user = $cookieStore.get('user');
         }
    var googleClientId ='1098530450027-kugelcr27orsjmkjc0h0h1qju66ru9jd.apps.googleusercontent.com';	    
     jrgGoogleAuth.init({'client_id':googleClientId, 'scopeHelp':['login', 'email', 'contacts']});    

    var evtGoogleLogin ="evtGoogleLogin";
    $scope.googleLogin =function() {
         if($cookieStore.get('user')==null) {
             $scope.google = true;
		   jrgGoogleAuth.login({'extraInfo':{'user_id':true, 'emails':true}, 'callback':{'evtName':evtGoogleLogin, 'args':[]} });
         }
        else{
              $scope.logged = true;
              console.log($scope.user);
              $cookieStore.put('user', $scope.user);
          }
	};
    $scope.googleInfo;
    $scope.$on(evtGoogleLogin, function(evt, googleInfo) {
		$scope.googleInfo =googleInfo;
        console.log(googleInfo);
        $scope.user.email=googleInfo.extraInfo.emailPrimary;
        $scope.user.pic = googleInfo.rawData.image.url;
		$scope.pic = googleInfo.rawData.image.url;
        $scope.user.fname = googleInfo.rawData.name.givenName;
        $scope.user.lname = googleInfo.rawData.name.familyName;
         $scope.logged = true;
         $http({
		        method : 'POST',
		        data : JSON.stringify($scope.user),
		        url : 'http://localhost:8080/trip/TokenService/tokens',
		        headers : {
		            'Content-Type' : 'application/json'
		        }
		    }).success(function(response){
		    	 $scope.user.token=response;
		    	
		    	$http({
	 		        method : 'POST',
	 		        data : JSON.stringify($scope.user),
	 		        url : 'http://localhost:8080/trip/TokenService/getalltrips',
	 		        headers : {
	 		            'Content-Type' : 'application/json'
	 		        }
	 		    }).success(function(response){
	 		    	$scope.userTrips=[];
	 		    	for(var i=0;i<=response.length-1;i++){
	 		    		$scope.trip=response[i];
	 		    		 $scope.userTrips.push($scope.trip);	
	 		    	}
	 		    	 $scope.tripSaved= true;
	 		    });
		    });
          $cookieStore.put('user', $scope.user);
        $scope.getContacts();
	});
      // Define user empty data :/
     
        $scope.shortlistedPlaces=[];
    	$scope.getContacts =function() {
		var promise1 =jrgGoogleAuth.getContacts();
		/*
		@param {Object} data
			@param {Array} contacts
				@param {String} name
				@param {String} email
				@param {String} phone
		*/
		promise1.then(function(data) {
			//for now, strip out contacts who don't have an email address (since currently aren't allowing users without an email address to be created / followed)
			var ii;
			var finalContacts =[];
			for(ii =0; ii<data.contacts.length; ii++) {
				if(data.contacts[ii].email !==undefined && data.contacts[ii].email) {
					finalContacts.push(data.contacts[ii]);
				}
			}
			// finalContacts =jrgArray.sort2D(finalContacts, 'name', {});		//sort by name
			$scope.googleContacts =finalContacts;
		}, function(data) {
			$scope.$emit('evtAppalertAlert', {type:'error', msg:'Error getting Google Contacts'});
		});
	};
        
        
      $scope.friends =[];
      $scope.init = function(){
          $scope.IntentLogin();
          return "string"
      };
       $scope.addFriendToTrip = function(){
       $scope.friends.push($scope.addFriends);
           $scope.addFriends="";
           console.log($scope.chosenPlace);
       };
      // Defining user logged status
      $scope.logged = false;
    
       $scope.getLatandLong = function(){
           var address = "New Delhi"
           $.ajax({
            url:"http://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=false",
            type: "POST",
            success:function(res){
            console.log(res.results[0].geometry.location.lat);
            console.log(res.results[0].geometry.location.lng);
        }
            });
       };
        
      // And some fancy flags to display messages upon user status change
      $scope.byebye = false;
      $scope.salutation = false;
      
      /**
       * Watch for Facebook to be ready.
       * There's also the event that could be used
       */
      $scope.$watch(
        function() {
          return Facebook.isReady();
        },
        function(newVal) {
          if (newVal)
            $scope.facebookReady = true;
        }
      );
      
      $scope.userIsConnected = false;
      
      Facebook.getLoginStatus(function(response) {
        if (response.status == 'connected') {
          $scope.userIsConnected = true;
        }
      });
      
      /**
       * IntentLogin
       */
      $scope.IntentLogin = function() {
          $scope.google=false;
        if($cookieStore.get('user')==null) {
          $scope.login();
         
        }
          else{
              $scope.logged = true;
              
            $scope.me();
        
            console.log($scope.user);
            $cookieStore.put('user', $scope.user);
          }
         
      };
      
      /**
       * Login
       */
       $scope.login = function() {
         Facebook.login(function(response) {
          if (response.status == 'connected') {
            $scope.logged = true;
            $scope.me();
            $scope.user.pic=  $scope.pic;
            console.log($scope.user);
			$scope.google=false;
			
			$http({
 		        method : 'POST',
 		        data : JSON.stringify($scope.user),
 		        url : 'http://localhost:8080/trip/TokenService/getalltrips',
 		        headers : {
 		            'Content-Type' : 'application/json'
 		        }
 		    }).success(function(response){
 		    	$scope.userTrips=[];
 		    	for(var i=0;i<response.length-1;i++){
 		    		$scope.trip=response[i];
 		    		 $scope.userTrips.push($scope.trip);	
 		    	}
 		    	 $scope.tripSaved= true;
 		    });
            $cookieStore.put('user', $scope.user);
            
          }
        
        },{scope:'email,user_friends'});
       };
       
       /**
        * me 
        */
        $scope.me = function() {
          Facebook.api('/me', function(response) {
            /**
             * Using $scope.$apply since this happens outside angular framework.
             */
            $scope.$apply(function() {
              $scope.user = response;
                 console.log($scope.user);
                 var users ={
  					   "id":$scope.user.id,
  					   "fname":$scope.user.first_name,
  					   "lname":$scope.user.last_name,
  					   "email":$scope.user.email,
  					   "token":""
      			}
  			   console.log(users);
                $http({
  			        method : 'POST',
  			        data : JSON.stringify(users),
  			        url : 'http://localhost:8080/trip/TokenService/tokens',
  			        headers : {
  			            'Content-Type' : 'application/json'
  			        }
  			    }).success(function(response){
  			    	 $scope.user = users;
  			    	 $scope.user.token=response;
  			    	
  			    	$http({
  		 		        method : 'POST',
  		 		        data : JSON.stringify($scope.user),
  		 		        url : 'http://localhost:8080/trip/TokenService/getalltrips',
  		 		        headers : {
  		 		            'Content-Type' : 'application/json'
  		 		        }
  		 		    }).success(function(response){
  		 		    	$scope.userTrips=[];
  		 		    	for(var i=0;i<=response.length-1;i++){
  		 		    		$scope.trip=response[i];
  		 		    		 $scope.userTrips.push($scope.trip);	
  		 		    	}
  		 		    	 $scope.tripSaved= true;
  		 		    });
  			    });
            });
            
          });
          Facebook.api('/me/picture?type=normal',function(picResponse){
              $scope.pic = picResponse.data.url;
          });
              Facebook.api('/me/friends',function(picResponse){
              console.log(picResponse.data);
          });
        };
      
      /**
       * Logout
       */
      $scope.logout = function() {
          if($scope.google==false){
        Facebook.logout(function() {
          $scope.$apply(function() {
            $scope.user   = {};
            $scope.logged = false;  
            $scope.userIsConnected = true;
            $cookieStore.remove('user');
          });
        });
          }
          else{
              gapi.auth.signOut();
               $scope.user   = {};
              $scope.logged = false;  
              $scope.userIsConnected = true;
              $cookieStore.remove('user');
          }
      }
      
      /**
       * Taking approach of Events :D
       */
      $scope.$on('Facebook:statusChange', function(ev, data) {
        console.log('Status: ', data);
        if (data.status == 'connected') {
          $scope.$apply(function() {
            $scope.salutation = true;
            $scope.byebye     = false;    
          });
        } else {
          $scope.$apply(function() {
            $scope.salutation = false;
            $scope.byebye     = true;
            
            // Dismiss byebye message after two seconds
            $timeout(function() {
              $scope.byebye = false;
            }, 2000)
          });
        }
        
        
      });
      
	  $scope.getSearchResultforDestination = function(){
		   $http.get('https://api.foursquare.com/v2/venues/explore?client_id=3MPTZ1NTWUJCWQS41TG5SGUK0QOZKA1HP41HVK5XDZR41ZPZ&client_secret=PVJRFGXHQWUM14T3PY0Y54ODEIHBMVD3WLENXGUQ5SK42THN&radius=500&venuePhotos=1&v=20140601&near='+$scope.trip.destinationPlace).
        success(function(data) {
            console.log(data);
			$scope.searchResult = data.response.groups[0].items;
        });
	  };
      
	  $scope.addToSelection = function(item){
		  $scope.shortlistedPlaces.push(item);
	  };
        
      $scope.validate = function(){
      var result = true;
          if($scope.trip.name ==null || $scope.trip.name ==undefined || $scope.trip.name==""){
          $scope.trip.nameStatusClass ="has-error";
          $scope.trip.nameError = "Name should not be empty";   
           result=false;
          }
          if($scope.trip.aboutTrip==null || $scope.trip.aboutTrip==undefined || $scope.trip.aboutTrip==""){
          $scope.trip.aboutTripStatusClass ="has-error";
          $scope.trip.aboutError = "Tell something about your trip";  
              result = false;
          }
           if($scope.trip.destination==null || $scope.trip.destination==undefined || $scope.trip.destination==""){
          $scope.trip.destStatusClass ="has-error";
          $scope.trip.destError = "tell us where you are planning to go.";    
               result= false;
          }
           if($scope.trip.memberEmail==null || $scope.trip.memberEmail==undefined || $scope.trip.memberEmail==""){
          $scope.trip.emailStatusClass ="has-error";
          $scope.trip.emailError = "why you are alone for a trip, invite your friends.";      
               result= false;
          }
           if($scope.trip.messageToFriends==null || $scope.trip.messageToFriends==undefined || $scope.trip.messageToFriends==""){
          $scope.trip.msgToFriendsStatusClass ="has-error";
          $scope.trip.msgToFriendsError = "it would be good if you send some message to your friends";      
               result = false;
         }
          
              return result;
      };
      
      $scope.suggestItems = function(place){
    	  $scope.suggestProgress = true;
        var discuss ={
        id:$scope.trip.discussion.length+1,
        user:$scope.user.fname+" "+$scope.user.lname,
        pic:$scope.pic,
        post:'has suggested '+place.venue.name,
        dateTime:new Date()
        };
        $scope.trip.discussion.push(discuss);
        var suggestion ={
           "id":place.venue.id,
           "photo":place.venue.photos.groups[0].items[0].prefix +"width500"+place.venue.photos.groups[0].items[0].suffix,
           "name":place.venue.name,
           "address1":place.venue.location.formattedAddress[0],
           "address2":place.venue.location.formattedAddress[1],
           "address3":place.venue.location.formattedAddress[2],
           "status":place.venue.hours.status
        }
        $scope.trip.suggestion.push(suggestion);
        $http({
	        method : 'POST',
	        data : JSON.stringify($scope.trip),
	        url : 'http://localhost:8080/trip/TokenService/trip',
	        headers : {
	            'Content-Type' : 'application/json'
	        }
	    }).success(function(response){
	    	$scope.trip=response;
	    });
        $scope.suggestProgress = false;
      };  
      $scope.createTrip = function(){
          if($scope.validate()){
          $scope.tripSaved = true;
          var fromDate = $scope.trip.date.startDate._d;
          var endDate = $scope.trip.date.endDate._d;
          $scope.trip.fromDate = fromDate;
          $scope.trip.toDate = endDate;
          $scope.trip.userEmail=$scope.user.email;
          $scope.trip.suggestion=[];
          $scope.trip.friends =[];
          var friendsArr= $scope.trip.memberEmail.split(";");
          for(var i=0;i<=friendsArr.length-1;i++){
        	  var friends ={
        			  email:friendsArr[i]
        	  				};
        	  $scope.trip.friends.push(friends);
          }
          var useremail ={
    			  email:$scope.user.email
    	  				};
          $scope.trip.friends.push(useremail)
          $scope.trip.discussion=[{
          id:1,
          user:$scope.user.fname+" "+$scope.user.lname,
          pic:$scope.pic,
          post:'has created '+$scope.trip.name,
          dateTime:new Date()
          }];
          $http({
		        method : 'POST',
		        data : JSON.stringify($scope.trip),
		        url : 'http://localhost:8080/trip/TokenService/trip',
		        headers : {
		            'Content-Type' : 'application/json'
		        }
		    }).success(function(response){
		    	$scope.trip=response;
		    	 $scope.userTrips.push($scope.trip);
		    });
         
          }
      };
      
     $scope.createPost = function(){
    	 $scope.showpostprogress=true;
    	var discussion = {
    	          id:($scope.trip.discussion.length+1),
    	          user:$scope.user.fname+" "+$scope.user.lname,
    	          pic:$scope.pic,
    	          post:$scope.thoughts,
    	          dateTime:new Date()
    	          }; 
    	$scope.trip.discussion.push(discussion);
    	$http({
	        method : 'POST',
	        data : JSON.stringify($scope.trip),
	        url : 'http://localhost:8080/trip/TokenService/trip',
	        headers : {
	            'Content-Type' : 'application/json'
	        }
	    }).success(function(response){
	    	$scope.trip=response;
	    	$scope.showpostprogress=false;
	    	});
    	$scope.thoughts = "";
     };
     $scope.createTrips = function(){
    	 $scope.tripSaved = false; 
    	 $scope.trip= null;
     };
     $scope.gotoTrip= function(trip){
    	 $scope.showprogress=true;
    	 $http({
		        method : 'POST',
		        data : JSON.stringify(trip),
		        url : 'http://localhost:8080/trip/TokenService/gettrip',
		        headers : {
		            'Content-Type' : 'application/json'
		        }
		    }).success(function(response){
		    	$scope.trip=response;
		    	$scope.showprogress=false;
		    });
    	 $scope.tripSaved = true;
    	 $scope.tripHeader =trip.name;
    	 
     };
     $scope.getAllTrip = function(){
    	 $scope.userTrips=[];
    	 $http({
 		        method : 'POST',
 		        data : JSON.stringify($scope.user),
 		        url : 'http://localhost:8080/trip/TokenService/getalltrips',
 		        headers : {
 		            'Content-Type' : 'application/json'
 		        }
 		    }).success(function(response){
 		    	for(var i=0;i<=response.length-1;i++){
 		    		$scope.trip=response[i];
 		    		 $scope.userTrips.push($scope.trip);	
 		    	}
 		    	 $scope.tripSaved= true;
 		    });
     }
     $scope.commentMe = function(discuss){
    	discuss.enableComment= true; 
     };
    
     $scope.saveComment = function(discuss){
    	 if(discuss.comment==undefined){
        	 discuss.comment=[];
         }
    	 var comments={
    			 id:discuss.comment.length,
    			 name:$scope.user.fname+" "+$scope.user.lname,
    			 pic:$scope.pic,
    			 comment:discuss.userComment
    	 };
    	 discuss.comment.push(comments);
    	 discuss.userComment="";
    	 $http({
 	        method : 'POST',
 	        data : JSON.stringify($scope.trip),
 	        url : 'http://localhost:8080/trip/TokenService/trip',
 	        headers : {
 	            'Content-Type' : 'application/json'
 	        }
 	    }).success(function(response){
 	    	$scope.trip=response;
 	    	$scope.showpostprogress=false;
 	    	});
     }
    }
  ]);